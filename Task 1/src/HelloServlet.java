// To save as "<TOMCAT_HOME>\webapps\hello\WEB-INF\classes\HelloServlet.java"
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.io.*;
import java.util.concurrent.ThreadLocalRandom;
 
public class HelloServlet extends HttpServlet {
	
	public static byte[] decodeImage(String imageDataString) {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedByteArray = decoder.decode(imageDataString);
        return decodedByteArray;
    }
	
	public void resize(String imagePath,
            String resizedImagePath, int width, int height)
            throws IOException {
       
        File inputFile = new File(imagePath);
        
        BufferedImage inputImage = ImageIO.read(inputFile);
        BufferedImage outputImage = new BufferedImage(width, height, inputImage.getType());
        
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, width, height, null);
        g2d.dispose();
 
        String formatName = resizedImagePath.substring(resizedImagePath.lastIndexOf(".") + 1);
 
        ImageIO.write(outputImage, formatName, new File(resizedImagePath));
    }
   @Override   
   public void doPost(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {

	   String onevalue= "src/default.jpg";   
	   if(request.getParameterMap().containsKey("numefisier")!=false) 
	   {
		   onevalue=request.getParameter("numefisier").toString();
	   }
	   
	   String content="";
	   if(request.getParameterMap().containsKey("fisier")!=false) 
	   {
		   content=request.getParameter("fisier");
		   byte[] imageByteArray = decodeImage(content);
		   int randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           // Write a image byte array into file system
           FileOutputStream imageOutFile = new FileOutputStream(
                   "src/"+onevalue+Integer.toString(randomNum)+"-copy.jpg");

           imageOutFile.write(imageByteArray);
           imageOutFile.close();
           randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           resize("src/"+onevalue+"-copy.jpg", "src/"+onevalue+Integer.toString(randomNum)+"-copy-500.jpg",500,500);
           randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           resize("src/"+onevalue+"-copy.jpg", "src/"+onevalue+Integer.toString(randomNum)+"-copy-400.jpg",400,400);
           randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           resize("src/"+onevalue+"-copy.jpg", "src/"+onevalue+Integer.toString(randomNum)+"-copy-300.jpg",300,300);
           randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           resize("src/"+onevalue+"-copy.jpg", "src/"+onevalue+Integer.toString(randomNum)+"-copy-200.jpg",200,200);
           randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           resize("src/"+onevalue+"-copy.jpg", "src/"+onevalue+Integer.toString(randomNum)+"-copy-100.jpg",100,100);
           randomNum = ThreadLocalRandom.current().nextInt(0, 1000);
           resize("src/"+onevalue+"-copy.jpg", "src/"+onevalue+Integer.toString(randomNum)+"-copy-50.jpg",50,50);
	   }
	      
      // Set the response MIME type of the response message
      response.setContentType("text/html");
      // Allocate a output writer to write the response message into the network socket
      PrintWriter out = response.getWriter();
 
      // Write the response message, in an HTML page
      try {
         out.println("SERVER: Received "+ content.length()+" bytes");
      } finally {
         out.close();  // Always close the output writer
      }
   }
}